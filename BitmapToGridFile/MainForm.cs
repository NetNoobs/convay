﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Convay.Core;

namespace BitmapToGridFile
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Bitmap image = new Bitmap(Image.FromFile(openFileDialog1.FileName));
                int width = image.Width;
                int height = image.Height;
                Grid<Convay.Core.Point> grid = new Grid<Convay.Core.Point>(width, height);
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        if (image.GetPixel(i, j).GetBrightness() > 0.5f)
                        {
                            grid[i, j] = false;
                        }
                        else
                        {
                            grid[i, j] = true;
                        }
                    }
                }
                Engine eng = new Engine(grid, "23/3");
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    eng.SaveSimpleToStream(saveFileDialog1.OpenFile());
                }
            }
        }
    }
}
