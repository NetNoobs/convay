﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 *      TODO:
 *      Poprawić dokumentację
 */

namespace Convay.Core
{
    public struct ByteGrid : IConvayGrid
    {
        public byte[,] byteArray;

        public ByteGrid(int xSize, int ySize)
        {
            byteArray = new byte[xSize, ySize];
            this.XSize = xSize;
            this.YSize = ySize;
        }

        public bool this[int xCoordinate, int yCoordinate]
        {
            get
            {
                byte actuallyState = Convert.ToByte(byteArray[xCoordinate, yCoordinate] & 1);
                return actuallyState != 0;
            }
            set
            {
                byteArray[xCoordinate, yCoordinate] = (byte)(byteArray[xCoordinate, yCoordinate] << 1);
                if (value)
                {
                    byteArray[xCoordinate, yCoordinate]++;
                }
            }
        }
        public int XSize { get; private set; }
        public int YSize { get; private set; }

        public bool Exist(int xCoordinate, int yCoordinate)
        {
            return !(xCoordinate < 0 || yCoordinate < 0 || xCoordinate >= XSize || yCoordinate >= YSize);
        }

        public IConvayGrid GetClone()
        {
            ByteGrid clone = new ByteGrid(XSize, YSize);
            clone.byteArray = (byte[,])byteArray.Clone();
            return clone;
        }
    }
}
