﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Convay.Core
{
    /// <summary>
    /// Wyjątek silnika gry
    /// </summary>
    public class EngineException : ConvayException
    {
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        public EngineException()
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        public EngineException(string message)
            : base(message)
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        /// <param name="innerException">Wyjątek wewnętrzny</param>
        public EngineException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
