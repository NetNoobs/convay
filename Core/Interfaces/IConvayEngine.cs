﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Convay.Core
{    
    /// <summary>
    /// Silnik gry w życie
    /// </summary>
    public interface IConvayEngine
    {
        event EventHandler StepTaken;
        event EventHandler RulesChanged;
        /// <summary>
        /// Właściwość umożliwiająca ustawienie lub uzyskanie zasad gry danej instancji w opowiednio sformatowanym stringu
        /// </summary>
        string GameRules
        {
            get;
            set;
        }
        /// <summary>
        /// Właściwość umożliwiająca ustawienie lub uzyskanie zasad gry danej instancji dotyczących żywych punktów
        /// </summary>
        string LifeRule
        {
            get;
            set;
        }
        /// <summary>
        /// Właściwość umożliwiająca ustawienie lub uzyskanie zasad gry danej instancji dotyczących martwych punktów
        /// </summary>
        string DeathRule
        {
            get;
            set;
        }
        /// <summary>
        /// Właściwość zwracająca/ustawiająca wewnętrzną siatkę silnika
        /// </summary>
        IConvayGrid Grid { get; set; }
        /// <summary>
        /// Zlicza żywych sąsiadów punktu
        /// </summary>
        /// <param name="xCoordinate">Współrzędna x</param>
        /// <param name="yCoordinate">Współrzędna y</param>
        /// <returns>liczba żywych sąsiadów</returns>
        int CountAliveNeighbours(int xCoordinate, int yCoordinate);
        /// <summary>
        /// Zlicza martwych sąsiadów punktu
        /// </summary>
        /// <param name="xCoordinate">Współrzędna x</param>
        /// <param name="yCoordinate">Współrzędna y</param>
        /// <returns>liczba martwych sąsiadów</returns>
        int CountDeadNeighbours(int xCoordinate, int yCoordinate);
        /// <summary>
        /// Wykonuje jeden krok gry
        /// </summary>
        /// <returns>Siatka po wykonaniu kroku</returns>
        IConvayGrid TakeStep();
        /// <summary>
        /// Wykonuje n-kroków gry
        /// </summary>
        /// <param name="n">ilość kroków</param>
        /// <returns>Siatka po wykonaniu kroków</returns>
        IConvayGrid TakeSteps(int n);
        void RandomizeGrid();
    }
}
