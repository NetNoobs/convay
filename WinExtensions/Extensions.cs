﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Convay.Core;
using System.Drawing.Imaging;

namespace Convay.WinExtensions
{
    public delegate byte ByteFromGrid(int x, int y);
    public static class Extensions
    {
        public static Bitmap ToBitmap(this IConvayGrid grid, Color livingColor, Color deadColor, bool useUnsafe)
        {
            int width = grid.XSize;
            int height = grid.YSize;
            int pixelSize = 4;
            Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            BitmapData bmpData = null;
            Color colorToSet = deadColor;
            if (useUnsafe)
            {
                bmpData = bitmap.LockBits(new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            }
            unsafe
            {
                for (int y = 0; y < height; y++)
                {
                    byte* row = null;
                    if (useUnsafe)
                    {
                        row = (byte*)bmpData.Scan0 + (y * bmpData.Stride);
                    }

                    for (int x = 0; x < width; x++)
                    {
                        if (grid[x, y])
                            colorToSet = livingColor;
                        else
                            colorToSet = deadColor;
                        if (useUnsafe)
                        {
                            row[x * pixelSize] = colorToSet.B;
                            row[x * pixelSize + 1] = colorToSet.G;
                            row[x * pixelSize + 2] = colorToSet.R;
                            row[x * pixelSize + 3] = colorToSet.A;
                        }
                        else
                        {
                            bitmap.SetPixel(x, y, colorToSet);
                        }
                    }
                }
            }
            if (useUnsafe)
            {
                bitmap.UnlockBits(bmpData);
            }
            return bitmap;
        }

        public static Bitmap ToBitmap(this IConvayGrid grid, bool useUnsafe)
        {
            return ToBitmap(grid, Color.Black, Color.White, useUnsafe);
        }

        public static Bitmap ToColorfulBitmap(this IConvayGrid grid, Color[] livingColors, Color[] deadColors,
            ByteFromGrid getByte, bool useUnsafe)
        {
            if (livingColors.Length < 8 || livingColors.Length < 8)
            {
                throw new ArgumentException("Tablica kolorów musi zawierać przynajmniej 8 kolorów!");
            }
            int width = grid.XSize;
            int height = grid.YSize;
            Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            BitmapData bmpData = null;
            if (useUnsafe)
            {
                bmpData = bitmap.LockBits(new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            }

            int pixelSize = 4;
            int highestLevel;
            int conditionNumber;
            Color[] conditionColor;

            unsafe
            {
                for (int y = 0; y < height; y++)
                {
                    byte* row = null;
                    if (useUnsafe)
                    {
                        row = (byte*)bmpData.Scan0 + (y * bmpData.Stride);
                    }

                    for (int x = 0; x < width; x++)
                    {
                        highestLevel = 0;
                        if (grid[x, y])
                        {
                            conditionNumber = 1;
                            conditionColor = livingColors;
                        }
                        else
                        {
                            conditionNumber = 0;
                            conditionColor = deadColors;
                        }
                        byte stateByte = getByte(x, y);
                        while (stateByte % 2 == conditionNumber)
                        {
                            stateByte = (byte)(stateByte >> 1);
                            highestLevel++;
                            if (highestLevel == 8)
                                break;
                        }
                        Color colorToSet = conditionColor[--highestLevel];
                        if (useUnsafe)
                        {
                            row[x * pixelSize] = colorToSet.B;
                            row[x * pixelSize + 1] = colorToSet.G;
                            row[x * pixelSize + 2] = colorToSet.R;
                            row[x * pixelSize + 3] = colorToSet.A;
                        }
                        else
                        {
                            bitmap.SetPixel(x, y, colorToSet);
                        }
                    }
                }
            }
            if (useUnsafe)
            {
                bitmap.UnlockBits(bmpData);
            }
            return bitmap;
        }
    }
}