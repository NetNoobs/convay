﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 *      TODO:
 *      Poprawić dokumentację
 */

namespace Convay.Core
{
    public class ByteEngine : Engine
    {
        public ByteEngine(IConvayGrid grid, string gameRules)
            : base(grid, gameRules)
        {
        }
        public ByteEngine(int xSize, int ySize, string gameRules)
            : base(new ByteGrid(xSize, ySize), gameRules)
        {
        }
        public override IConvayGrid TakeStep()
        {
            if (!(grid is ByteGrid))
            {
                return base.TakeStep();
            }
            ByteGrid gridNew = (ByteGrid)grid.GetClone();
            int living;
            char livingCh;
            int xSize = grid.XSize;
            int ySize = grid.YSize;
            for (int i = 0; i < xSize; i++)
            {
                for (int j = 0; j < ySize; j++)
                {
                    living = 0;
                    if (i > 0 && j > 0 && i < xSize - 1 && j < ySize - 1)
                    {
                        if ((((ByteGrid)grid).byteArray[i + 1, j + 1] & 1) == 1)
                            living++;
                        if ((((ByteGrid)grid).byteArray[i, j + 1] & 1) == 1)
                            living++;
                        if ((((ByteGrid)grid).byteArray[i - 1, j + 1] & 1) == 1)
                            living++;
                        if ((((ByteGrid)grid).byteArray[i + 1, j] & 1) == 1)
                            living++;
                        if ((((ByteGrid)grid).byteArray[i - 1, j] & 1) == 1)
                            living++;
                        if ((((ByteGrid)grid).byteArray[i + 1, j - 1] & 1) == 1)
                            living++;
                        if ((((ByteGrid)grid).byteArray[i, j - 1] & 1) == 1)
                            living++;
                        if ((((ByteGrid)grid).byteArray[i - 1, j - 1] & 1) == 1)
                            living++;
                    }
                    else
                    {
                        living = base.CountAliveNeighbours(i, j);
                    }

                    livingCh = Convert.ToChar(living.ToString());
                    if (grid[i, j])
                    {
                        gridNew[i, j] = lifeRule.Contains(livingCh);
                    }
                    else
                    {
                        gridNew[i, j] = deathRule.Contains(livingCh);
                    }
                }
            }
            grid = gridNew;
            OnStepTaken(new EventArgs());
            return grid;
        }
    }
}
