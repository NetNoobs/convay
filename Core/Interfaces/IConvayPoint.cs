﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Convay.Core
{
    /// <summary>
    /// Pojedynczy obiekt punktu gry w życie
    /// </summary>
    public interface IConvayPoint
    {
        /// <summary>
        /// Ustawienie lub Zwrócenie aktualnego stanu
        /// </summary>
        bool SimpleState
        {
            get;
            set;
        }
        /// <summary>
        /// Klonowanie punktu
        /// </summary>
        /// <returns>Sklonowany egzemplarz punktu</returns>
        IConvayPoint Clone();
    }
}