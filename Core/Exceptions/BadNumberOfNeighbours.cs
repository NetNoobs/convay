﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Convay.Core
{
    /// <summary>
    /// Wyjątek silnika gry rzucany w wypadku błędnej liczby zliczonych sąsiadów
    /// </summary>
    public class BadNumberOfNeighbours : EngineException
    {
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        public BadNumberOfNeighbours()
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        public BadNumberOfNeighbours(string message)
            : base(message)
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        /// <param name="innerException">Wyjątek wewnętrzny</param>
        public BadNumberOfNeighbours(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
