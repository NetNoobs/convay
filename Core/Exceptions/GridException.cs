﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Convay.Core
{
    /// <summary>
    /// Wyjątek błędu siatki
    /// </summary>
    public class GridException : ConvayException
    {
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        public GridException()
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        public GridException(string message)
            : base(message)
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        /// <param name="innerException">Wyjątek wewnętrzny</param>
        public GridException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
