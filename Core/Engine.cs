﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 *      TODO:
 *      Teksty wyjątków umieścić w Resources i uzależnić od kultury
 *      Poprawić dokumentację
 */

namespace Convay.Core
{
    /// <summary>
    /// Silnik gry w życie
    /// </summary>
    public class Engine : IConvayEngine
    {
        protected List<char> lifeRule, deathRule;
        protected IConvayGrid grid;
        protected Random randomizer;

        public virtual event EventHandler StepTaken;
        public virtual event EventHandler RulesChanged;

        /// <summary>
        /// Konstruktor bezparametrowy, przypisuje tylko wewnętrzne pole grid
        /// </summary>
        /// <param name="grid">Siatka gry</param>
        private Engine(IConvayGrid grid)
        {
            this.grid = grid;
            randomizer = new Random();
            lifeRule = new List<char>();
            deathRule = new List<char>();
        }
        /// <summary>
        /// Sprawdza, czy punkt żyje.
        /// </summary>
        /// <param name="x">Współrzędna x</param>
        /// <param name="y">Współrzędna y</param>
        /// <returns>Jeżeli tak, zwraca '1', jeżeli nie lub wykracza poza zakres - zwraca '0'</returns>
        virtual protected int Alive(int x, int y)
        {
            if (!grid.Exist(x, y))
            {
                return 0;
            }
            else if (grid[x, y])
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        virtual protected void OnRulesChanged(EventArgs e)
        {
            if (RulesChanged != null)
                RulesChanged(this, e);
        }
        virtual protected void OnStepTaken(EventArgs e)
        {
            if (StepTaken != null)
                StepTaken(this, e);
        }
        /// <summary>
        /// Ustawia podaną listę charów jako zasady gry na podstawie podanego stringa w odpowiednim formacie
        /// </summary>
        /// <param name="ruleList">Lista charów, do której mają być wczytane zasady</param>
        /// <param name="ruleString">String z zasadami w odpowiednim formacie</param>
        protected void SetRule(ICollection<char> ruleList, string ruleString)
        {
            ruleList.Clear();
            for (int i = 0; i < ruleString.Length; i++)
            {
                char ch = ruleString[i];
                if (ch >= '0' && ch <= '9')
                {
                    ruleList.Add(ch);
                }
                else
                {
                    throw new BadRulesFormatException("Niedozwolone znaki w regułach gry (dopuszczalne [0-9]) i '/' jako separator)");
                }
            }
        }
        /// <summary>
        /// Konstruuje obiekt klasy Engine
        /// </summary>
        /// <param name="grid">Obiekt IConvayGrid na którym będzie operował silnik</param>
        /// <param name="gameRules">Zasady gry w odpowiednim formacie</param>
        public Engine(IConvayGrid grid, string gameRules)
            : this(grid)
        {
            GameRules = gameRules;
        }
        /// <summary>
        /// Konstruuje obiekt klasy Engine
        /// </summary>
        /// <param name="grid">Obiekt IConvayGrid na którym będzie operował silnik</param>
        /// <param name="lifeRule">String składający się z cyfr określający zasady gry dotyczących żywych punktów</param>
        /// <param name="deathRule">String składający się z cyfr określający zasady gry dotyczących martwych punktów</param>
        public Engine(IConvayGrid grid, string lifeRule, string deathRule)
            : this(grid)
        {
            LifeRule = lifeRule;
            DeathRule = deathRule;
        }
        /// <summary>
        /// Właściwość umożliwiająca ustawienie lub uzyskanie zasad gry danej instancji w opowiednio sformatowanym stringu
        /// </summary>
        public string GameRules
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(LifeRule);
                sb.Append('/');
                sb.Append(DeathRule);
                return sb.ToString();
            }
            set
            {
                string[] rules = value.Split('/');
                if (rules.Length != 2)
                {
                    throw new BadRulesFormatException("Nieprawidłowy format reguł (powinny być oddzielone jednym znakiem '/')");
                }
                LifeRule = rules[0];
                DeathRule = rules[1];
                OnRulesChanged(new EventArgs());
            }
        }
        /// <summary>
        /// Właściwość umożliwiająca ustawienie lub uzyskanie zasad gry danej instancji dotyczących żywych punktów
        /// </summary>
        public string LifeRule
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (var item in lifeRule)
                {
                    sb.Append(item);
                }
                return sb.ToString();
            }
            set
            {
                SetRule(lifeRule, value);
                OnRulesChanged(new EventArgs());
            }
        }
        /// <summary>
        /// Właściwość umożliwiająca ustawienie lub uzyskanie zasad gry danej instancji dotyczących martwych punktów
        /// </summary>
        public string DeathRule
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (var item in deathRule)
                {
                    sb.Append(item);
                }
                return sb.ToString();
            }
            set
            {
                SetRule(deathRule, value);
                OnRulesChanged(new EventArgs());
            }
        }
        /// <summary>
        /// Wykonuje jeden krok gry
        /// </summary>
        /// <returns>Siatka po wykonaniu kroku</returns>
        virtual public IConvayGrid TakeStep()
        {
            IConvayGrid gridNew = grid.GetClone();
            int living;
            char livingCh;
            for (int i = 0; i < grid.XSize; i++)
            {
                for (int j = 0; j < grid.YSize; j++)
                {
                    living = CountAliveNeighbours(i, j);
                    if (living > 9)
                    {
                        throw new BadNumberOfNeighbours("Ilość żywych sąsiadów wynosi ponad 9, reguły gry nie mają zastosowania!");
                    }
                    livingCh = Convert.ToChar(living.ToString());
                    if (grid[i, j])
                    {
                        gridNew[i,j] = lifeRule.Contains(livingCh);
                    }
                    else
                    {
                        gridNew[i, j] = deathRule.Contains(livingCh);
                    }
                }
            }
            grid = gridNew;
            OnStepTaken(new EventArgs());
            return grid;
        }
        /// <summary>
        /// Wykonuje n-kroków gry
        /// </summary>
        /// <param name="n">ilość kroków</param>
        /// <returns>Siatka po wykonaniu kroków</returns>
        virtual public IConvayGrid TakeSteps(int n)
        {
            for (int i = 0; i < n; i++)
            {
                TakeStep();
            }
            return grid;
        }
        /// <summary>
        /// Zlicza żywych sąsiadów punktu
        /// </summary>
        /// <param name="xCoordinate">Współrzędna x</param>
        /// <param name="yCoordinate">Współrzędna y</param>
        /// <returns>liczba żywych sąsiadów</returns>
        virtual public int CountAliveNeighbours(int xCoordinate, int yCoordinate)
        {
            if (!grid.Exist(xCoordinate, yCoordinate))
            {
                throw new OutOfGridRangeException("Odwołanie do nieistniejącego punktu siatki");
            }
            int count = 0;
            int leftSide = xCoordinate - 1;
            int rightSide = xCoordinate + 1;
            int topSide = yCoordinate - 1;
            int bottomSide = yCoordinate + 1;
            count += Alive(leftSide, topSide);
            count += Alive(xCoordinate, topSide);
            count += Alive(rightSide, topSide);
            count += Alive(leftSide, yCoordinate);
            count += Alive(rightSide, yCoordinate);
            count += Alive(leftSide, bottomSide);
            count += Alive(xCoordinate, bottomSide);
            count += Alive(rightSide, bottomSide);
            return count;
        }
        /// <summary>
        /// Zlicza martwych sąsiadów punktu
        /// </summary>
        /// <param name="xCoordinate">Współrzędna x</param>
        /// <param name="yCoordinate">Współrzędna y</param>
        /// <returns>liczba martwych sąsiadów</returns>
        virtual public int CountDeadNeighbours(int xCoordinate, int yCoordinate)
        {
            return 8 - CountAliveNeighbours(xCoordinate, yCoordinate);
        }
        /// <summary>
        /// Właściwość zwracająca/ustawiająca wewnętrzną siatkę silnika
        /// </summary>
        virtual public IConvayGrid Grid
        {
            get { return grid; }
            set { grid = value; }
        }
        /// <summary>
        /// Metoda losuje siatkę
        /// </summary>
        virtual public void RandomizeGrid()
        {
            for (int i = 0; i < grid.XSize; i++)
            {
                for (int j = 0; j < grid.YSize; j++)
                {
                    grid[i, j] = Convert.ToBoolean(randomizer.Next(0, 2));
                }
            }
        }
        /// <summary>
        /// Umożliwia zapisanie podstawowego stanu punktów siatki do strumienia
        /// </summary>
        /// <param name="stream">Strumień do którego dane mają być zapisane</param>
        public virtual void SaveSimpleToStream(System.IO.Stream stream)
        {
            System.IO.BinaryWriter writer = new System.IO.BinaryWriter(stream);
            int counter = 0;
            byte byteToWrite = 0;
            int xSize = grid.XSize;
            int ySize = grid.YSize;
            writer.Write(xSize);
            writer.Write(ySize);
            for (int i = 0; i < xSize; i++)
            {
                for (int j = 0; j < ySize; j++)
                {
                    byteToWrite = (byte)(byteToWrite << 1);
                    if (grid[i, j])
                    {
                        byteToWrite++;
                    }
                    counter++;
                    if (counter == 8)
                    {
                        counter = 0;
                        writer.Write(byteToWrite);
                        byteToWrite = 0;
                    }
                }
            }
            while (counter > 0)
            {
                byteToWrite = (byte)(byteToWrite << 1);
                counter++;
                if (counter == 8)
                {
                    counter = 0;
                    writer.Write(byteToWrite);
                    byteToWrite = 0;
                }
            }
        }
        /// <summary>
        /// Umożliwia załadowanie podstawowego stanu punktów siatki ze strumienia
        /// </summary>
        /// <param name="stream">Strumień z danymi o punktach</param>
        public virtual void LoadSimpleFromStream(System.IO.Stream stream)
        {
            System.IO.BinaryReader reader = new System.IO.BinaryReader(stream);
            int xSize = reader.ReadInt32();
            int ySize = reader.ReadInt32();
            int counter = 0;
            if (xSize == 0 && ySize == 0)
            {
                return;
            }
            grid = (IConvayGrid)Activator.CreateInstance(grid.GetType(), xSize, ySize);
            byte readByte = reader.ReadByte();
            for (int i = 0; i < xSize; i++)
            {
                for (int j = 0; j < ySize; j++)
                {
                    if (counter == 8)
                    {
                        readByte = reader.ReadByte();
                        counter = 0;
                    }
                    grid[i, j] = ((readByte & 128) == 128);
                    readByte = (byte)(readByte << 1);
                    counter++;
                }
            }
        }
    }
}
