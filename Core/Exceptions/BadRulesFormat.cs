﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Convay.Core
{
    /// <summary>
    /// Wyjątek silnika gry rzucany w wypadku podania nieprawidłowego formatu zasad
    /// </summary>
    public class BadRulesFormatException : EngineException
    {
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        public BadRulesFormatException()
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        public BadRulesFormatException(string message)
            : base(message)
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        /// <param name="innerException">Wyjątek wewnętrzny</param>
        public BadRulesFormatException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
