﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Convay.Core
{
    /// <summary>
    /// Wyjątek w bibliotece Convay.Core
    /// </summary>
    public class ConvayException : Exception
    {
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        public ConvayException()
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        public ConvayException(string message)
            : base(message)
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        /// <param name="innerException">Wyjątek wewnętrzny</param>
        public ConvayException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
