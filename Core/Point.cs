﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convay.Core
{
    /// <summary>
    /// Pojedynczy obiekt punktu gry w życie
    /// </summary>
    public struct Point : IConvayPoint
    {
        /// <summary>
        /// bitowy stan punktu wraz z historią
        /// </summary>
        private byte state;
        /// <summary>
        /// bitowy stan punktu wraz z historią
        /// </summary>
        public byte State
        {
            get { return state; }
            set { state = value; }
        }
        /// <summary>
        /// Ustawienie lub Zwrócenie aktualnego stanu
        /// </summary>
        public bool SimpleState
        {
            get
            {
                byte actuallyState = Convert.ToByte(state & 1);
                return actuallyState != 0;
            }
            set
            {
                state = (byte)(state << 1);
                if (value)
                {
                    state++;
                }
            }
        }
        /// <summary>
        /// Klonowanie punktu
        /// </summary>
        /// <returns>Sklonowany egzemplarz punktu</returns>
        public IConvayPoint Clone()
        {
            Point point = new Point();
            point.state = this.state;
            return point;
        }
    }
}
