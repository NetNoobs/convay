﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Convay.Core;
using Convay.WinExtensions;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/*
 *      TODO:
 *      Uładnić ten kod...
 */


namespace Convay.WinFormsGame
{
    public partial class MainForm : Form
    {
        Engine gameEngine;

        public MainForm()
        {
            InitializeComponent();
            Settings.engineType = typeof(ByteEngine);
            Settings.gridType = typeof(ByteGrid);
            gameEngine = new ByteEngine(null, "23/3");
            Settings.livingColors = new Color[8];
            Settings.deadColors = new Color[8];
            for (int i = 0; i < 8; i++)
            {
                Settings.livingColors[i] = Color.Black;
                Settings.deadColors[i] = Color.White;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            disableMenuIfGridNull();
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateGridForm dialog = new CreateGridForm();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (dialog.XSize > 0 && dialog.YSize > 0)
                {
                    gameEngine.Grid = (IConvayGrid)Activator.CreateInstance(Settings.gridType, dialog.XSize, dialog.YSize);
                }
                else
                {
                    MessageBox.Show(this, "Invalid size!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                drawGrid();
                disableMenuIfGridNull();
            }
        }

        private void disableMenuIfGridNull()
        {
            bool isNull = gameEngine.Grid == null;
            saveToolStripMenuItem.Enabled = !isNull;
            randomizeToolStripMenuItem.Enabled = !isNull;
            startToolStripMenuItem.Enabled = !isNull;
            stopToolStripMenuItem.Enabled = !isNull;
        }

        private void interpolationToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem item in (sender as ToolStripMenuItem).DropDown.Items)
            {
                item.Checked = false;
            }
            switch (interpolatedPictureBox1.Interpolation)
            {
                case System.Drawing.Drawing2D.InterpolationMode.Bicubic:
                    bicubicToolStripMenuItem.Checked = true;
                    break;
                case System.Drawing.Drawing2D.InterpolationMode.Bilinear:
                    bilinearToolStripMenuItem.Checked = true;
                    break;
                case System.Drawing.Drawing2D.InterpolationMode.High:
                    highToolStripMenuItem.Checked = true;
                    break;
                case System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic:
                    bicubicHQToolStripMenuItem.Checked = true;
                    break;
                case System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear:
                    bilinearHQToolStripMenuItem.Checked = true;
                    break;
                case System.Drawing.Drawing2D.InterpolationMode.Low:
                    lowToolStripMenuItem.Checked = true;
                    break;
                case System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor:
                    sharpToolStripMenuItem.Checked = true;
                    break;
                default:
                    break;
            }
        }

        private void interpolationDownStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if (menuItem != null)
            {
                switch (menuItem.Tag.ToString())
                {
                    case "tsmi/i/l":
                        interpolatedPictureBox1.Interpolation = System.Drawing.Drawing2D.InterpolationMode.Low;
                        break;
                    case "tsmi/i/h":
                        interpolatedPictureBox1.Interpolation = System.Drawing.Drawing2D.InterpolationMode.High;
                        break;
                    case "tsmi/i/bc":
                        interpolatedPictureBox1.Interpolation = System.Drawing.Drawing2D.InterpolationMode.Bicubic;
                        break;
                    case "tsmi/i/hqbc":
                        interpolatedPictureBox1.Interpolation = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        break;
                    case "tsmi/i/hqbr":
                        interpolatedPictureBox1.Interpolation = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
                        break;
                    case "tsmi/i/nn":
                        interpolatedPictureBox1.Interpolation = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                        break;
                    case "tsmi/i/br":
                        interpolatedPictureBox1.Interpolation = System.Drawing.Drawing2D.InterpolationMode.Bilinear;
                        break;
                    default:
                        break;
                }
            }
        }

        private void sizeModeDownStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if (menuItem != null)
            {
                switch (menuItem.Tag.ToString())
                {
                    case "tsmi/sm/c":
                        interpolatedPictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
                        break;
                    case "tsmi/sm/z":
                        interpolatedPictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                        break;
                    case "tsmi/sm/s":
                        interpolatedPictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                        break;
                    default:
                        break;
                }
            }
        }

        private void sizeModeToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem item in (sender as ToolStripMenuItem).DropDown.Items)
            {
                item.Checked = false;
            }
            switch (interpolatedPictureBox1.SizeMode)
            {
                case PictureBoxSizeMode.CenterImage:
                    normalToolStripMenuItem.Checked = true;
                    break;
                case PictureBoxSizeMode.StretchImage:
                    stretchToolStripMenuItem.Checked = true;
                    break;
                case PictureBoxSizeMode.Zoom:
                    zoomToolStripMenuItem.Checked = true;
                    break;
                default:
                    break;
            }
        }

        private void randomizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gameEngine.RandomizeGrid();
            drawGrid();
        }

        private void setRulesToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            gameRulesTextBox.Text = gameEngine.GameRules;
        }

        private void setRulesToolStripMenuItem_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                gameEngine.GameRules = gameRulesTextBox.Text;
            }
            catch (EngineException ex)
            {
                MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                gameRulesTextBox.Text = gameEngine.GameRules;
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (gameEngine.Grid == null)
                {
                    gameEngine.Grid = (IConvayGrid)Activator.CreateInstance(Settings.gridType, 0, 0);
                }
                FileStream stream = null;
                try
                {
                    stream = new FileStream(openFileDialog1.FileName, FileMode.Open);
                    gameEngine.LoadSimpleFromStream(stream);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                finally
                {
                    stream.Close();
                }
                drawGrid();
                disableMenuIfGridNull();
            }
        }

        private void drawGrid()
        {
            bool timerState = timer1.Enabled;
            if (timerState)
                timer1.Enabled = false;
            if (Settings.extendedDrawing)
            {
                if (gameEngine.Grid is Grid<Core.Point>)
                {
                    interpolatedPictureBox1.Image = gameEngine.Grid.ToColorfulBitmap(Settings.livingColors, Settings.deadColors,
                        (x, y) => { return ((Grid<Core.Point>)gameEngine.Grid).GetPoint(x, y).State; }, Settings.unsafeDrawing);
                }
                else if (gameEngine.Grid is ByteGrid)
                {
                    interpolatedPictureBox1.Image = gameEngine.Grid.ToColorfulBitmap(Settings.livingColors, Settings.deadColors,
                        (x, y) => { return ((ByteGrid)gameEngine.Grid).byteArray[x, y]; }, Settings.unsafeDrawing);
                }
            }
            else
                interpolatedPictureBox1.Image = gameEngine.Grid.ToBitmap(Settings.livingColors[7], Settings.deadColors[7], Settings.unsafeDrawing);
            if (timerState)
                timer1.Enabled = true;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                FileStream stream = null;
                try
                {
                    stream = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    gameEngine.SaveSimpleToStream(stream);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    stream.Close();
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            backgroundWorker1.RunWorkerAsync();
        }

        private void intervalToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            intervalTextBoxToolStripMenuItem.Text = timer1.Interval.ToString();
        }

        private void intervalToolStripMenuItem_DropDownClosed(object sender, EventArgs e)
        {
            int interval = 0;
            if (int.TryParse(intervalTextBoxToolStripMenuItem.Text, out interval) && interval > 0)
            {
                timer1.Interval = interval;
            }
            else
            {
                MessageBox.Show(this, "Interval is invalid!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            State.gameIsRunning = true;
            timer1.Enabled = true;
            fileToolStripMenuItem.Enabled = false;
            gridToolStripMenuItem.Enabled = false;
            demoToolStripMenuItem.Enabled = false;
            startToolStripMenuItem.Enabled = false;
            settingsToolStripMenuItem.Enabled = false;
            demoToolStripMenuItem.Enabled = false;
            infoToolStripMenuItem.Visible = true;
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            State.gameIsRunning = false;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            gameEngine.TakeStep();
            drawGrid();
            State.fps++;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (State.gameIsRunning)
            {
                timer1.Enabled = true;
            }
            else
            {
                timer1.Enabled = false;
                fileToolStripMenuItem.Enabled = true;
                gridToolStripMenuItem.Enabled = true;
                demoToolStripMenuItem.Enabled = true;
                startToolStripMenuItem.Enabled = true;
                settingsToolStripMenuItem.Enabled = true;
                demoToolStripMenuItem.Enabled = true;
                infoToolStripMenuItem.Visible = false;
            }
        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AppearanceForm af = new AppearanceForm(Settings.livingColors, Settings.deadColors);
            if (af.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Settings.livingColors = af.LivingColors;
                Settings.deadColors = af.DeadColors;
            }
        }

        private void colorsToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            extendedToolStripMenuItem.Checked = Settings.extendedDrawing;
            simpleToolStripMenuItem.Checked = !Settings.extendedDrawing;
        }

        private void simpleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.extendedDrawing = false;
        }

        private void extendedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.extendedDrawing = true;
        }

        private void demoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.extendedDrawing = true;
            interpolatedPictureBox1.Interpolation = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            interpolatedPictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream(Properties.Resources.colorful);
            Settings.livingColors = bf.Deserialize(ms) as Color[];
            Settings.deadColors = bf.Deserialize(ms) as Color[];
            ms.Close();
            gameEngine.Grid = (IConvayGrid)Activator.CreateInstance(Settings.gridType, 250, 250);
            disableMenuIfGridNull();
            gameEngine.RandomizeGrid();
            this.WindowState = FormWindowState.Maximized;
            timer1.Interval = 1;
            startToolStripMenuItem_Click(null, null);
            demoToolStripMenuItem.Enabled = false;
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsForm sf = new SettingsForm();
            if (sf.ShowDialog() == DialogResult.OK)
            {
                switch (sf.Drawing)
                {
                    case SettingsForm.DrawingType.safeDrawing:
                        Settings.unsafeDrawing = false;
                        break;
                    case SettingsForm.DrawingType.unsafeDrawing:
                        Settings.unsafeDrawing = true;
                        break;
                    default:
                        break;
                }
                switch (sf.Grid)
                {
                    case SettingsForm.GridType.GridCorePoint:
                        Settings.gridType = typeof(Grid<Core.Point>);
                        break;
                    case SettingsForm.GridType.ByteGrid:
                        Settings.gridType = typeof(ByteGrid);
                        break;
                    default:
                        break;
                }
                switch (sf.Engine)
                {
                    case SettingsForm.EngineType.Engine:
                        Settings.engineType = typeof(Engine);
                        break;
                    case SettingsForm.EngineType.ByteEngine:
                        Settings.engineType = typeof(ByteEngine);
                        break;
                    default:
                        break;
                }
                gameEngine = (Engine)Activator.CreateInstance(Settings.engineType, gameEngine.Grid, gameEngine.GameRules);
            }
        }

        private void fpsCounter_Tick(object sender, EventArgs e)
        {
            fps.Text = State.fps + "fps";
            State.fps = 0;
        }

        private void enToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            if (gameEngine != null && gameEngine.Grid != null)
            {
            infoToolStripMenuItem.Text = "used engine: " + gameEngine.GetType() + " used grid: " + gameEngine.Grid.GetType() + " unsafe draw: " + Settings.unsafeDrawing;

            }
        }

        private void enToolStripMenuItem_MouseLeave(object sender, EventArgs e)
        {
            infoToolStripMenuItem.Text = "hover for info";
        }
    }
}
