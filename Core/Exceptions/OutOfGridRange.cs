﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Convay.Core
{
    /// <summary>
    /// Wyjątek rzucony w wypadku gdy nastąpiło odwołanie do punktu nieistniejącego w siatce gry
    /// </summary>
    public class OutOfGridRangeException : GridException
    {
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        public OutOfGridRangeException()
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        public OutOfGridRangeException(string message)
            : base(message)
        {
        }
        /// <summary>
        /// Konstruktor wyjątku
        /// </summary>
        /// <param name="message">Wiadomość przekazywana w wyjątku</param>
        /// <param name="innerException">Wyjątek wewnętrzny</param>
        public OutOfGridRangeException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
