﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Convay.WinFormsGame
{
    public partial class SettingsForm : Form
    {
        public enum EngineType
        {
            Engine,
            ByteEngine
        }
        public enum GridType
        {
            GridCorePoint,
            ByteGrid
        }
        public enum DrawingType
        {
            safeDrawing,
            unsafeDrawing
        }
        public SettingsForm()
        {
            InitializeComponent();
            engineStandard.Checked = Settings.engineType == typeof(Core.Engine);
            byteEngine.Checked = Settings.engineType == typeof(Core.ByteEngine);
            gridCorePointType.Checked = Settings.gridType == typeof(Core.Grid<Core.Point>);
            byteGridType.Checked = Settings.gridType == typeof(Core.ByteGrid);
            unsafeType.Checked = Settings.unsafeDrawing;
            safeType.Checked = !Settings.unsafeDrawing;
        }
        public EngineType Engine
        {
            get
            {
                EngineType et = 0;
                if (engineStandard.Checked)
                {
                    et = EngineType.Engine;
                }
                if (byteEngine.Checked)
                {
                    et = EngineType.ByteEngine;
                }
                return et;
            }
        }
        public GridType Grid
        {
            get
            {
                GridType grid = 0;
                if (gridCorePointType.Checked)
                {
                    grid = GridType.GridCorePoint;
                }
                if (byteGridType.Checked)
                {
                    grid = GridType.ByteGrid;
                }
                return grid;
            }
        }
        public DrawingType Drawing
        {
            get
            {
                DrawingType draw = 0;
                if (safeType.Checked)
                {
                    draw = DrawingType.safeDrawing;
                }
                if (unsafeType.Checked)
                {
                    draw = DrawingType.unsafeDrawing;
                }
                return draw;
            }
        }
    }
}
