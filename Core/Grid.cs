﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 *      TODO:
 *      Teksty wyjątków umieścić w Resources i uzależnić od kultury
 *      Poprawić dokumentację
 */

namespace Convay.Core
{
    /// <summary>
    /// Podstawowa siatka gry w życie
    /// </summary>
    /// <typeparam name="T">Typ punktów</typeparam>
    public class Grid<T> : IConvayGrid where T : IConvayPoint, new()
    {
        /// <summary>
        /// Tablica punktów T
        /// </summary>
        protected T[,] pointArray;
        /// <summary>
        /// Konstruuje siatkę o zadanej wielkości
        /// </summary>
        /// <param name="xSize">Szerokość</param>
        /// <param name="ySize">Wysokość</param>
        public Grid(int xSize, int ySize)
        {
            Clear(xSize, ySize);
        }
        /// <summary>
        /// Umożliwia dostęp do punktu o zadanych współrzędnych
        /// </summary>
        /// <param name="x">Współrzędna X</param>
        /// <param name="y">Współrzędna Y</param>
        /// <returns>Referencja danego punktu</returns>
        public virtual T GetPoint(int x, int y)
        {
            if (Exist(x, y))
            {
                return pointArray[x, y];
            }
            else
            {
                throw new OutOfGridRangeException("Koordynaty wykraczają poza wymiar siatki");
            }
        }
        /// <summary>
        /// Ustawia pustą siatkę o zadanej wielkości
        /// </summary>
        /// <param name="xSize">Szerokość</param>
        /// <param name="ySize">Wysokość</param>
        public virtual void Clear(int xSize, int ySize)
        {
            pointArray = new T[xSize, ySize];
            if (!typeof(T).IsValueType)
            {
                for (int i = 0; i < xSize; i++)
                {
                    for (int j = 0; j < ySize; j++)
                    {
                        pointArray[i, j] = new T();
                    }
                }
            }
        }
        /// <summary>
        /// Określa czy punkt jest żywy
        /// </summary>
        /// <param name="xCoordinate">Współrzędna x</param>
        /// <param name="yCoordinate">Współrzędna y</param>
        /// <returns>true gdy żywy, false gdy martwy</returns>
        public virtual bool this[int xCoordinate, int yCoordinate]
        {
            get
            {
                if (!Exist(xCoordinate, yCoordinate))
                {
                    throw new IndexOutOfRangeException("Koordynaty wykraczają poza wymiar siatki");
                }
                else
                {
                    return pointArray[xCoordinate, yCoordinate].SimpleState;
                }
            }
            set
            {
                if (!Exist(xCoordinate, yCoordinate))
                {
                    throw new IndexOutOfRangeException("Koordynaty wykraczają poza wymiar siatki");
                }
                else
                {
                    pointArray[xCoordinate, yCoordinate].SimpleState = value;
                }
            }
        }
        /// <summary>
        /// Zwraca szerokość siatki
        /// </summary>
        public virtual int XSize
        {
            get { return pointArray.GetLength(0); }
        }
        /// <summary>
        /// Zwraca wysokość siatki
        /// </summary>
        public virtual int YSize
        {
            get { return pointArray.GetLength(1); }
        }
        /// <summary>
        /// Zwraca kopię siatki
        /// </summary>
        /// <returns>kopia siatki</returns>
        public virtual IConvayGrid GetClone()
        {
            int xSize = XSize;
            int ySize = YSize;
            Grid<T> clonedGrid = new Grid<T>(xSize, ySize);
            for (int i = 0; i < xSize; i++)
            {
                for (int j = 0; j < ySize; j++)
                {
                    clonedGrid.pointArray[i, j] = (T)this.pointArray[i, j].Clone();
                }
            }
            return clonedGrid;
        }
        /// <summary>
        /// Określa istnienie punktu o zadanych współrzędnych w siatce
        /// </summary>
        /// <param name="xCoordinate">Współrzędna X</param>
        /// <param name="yCoordinate">Współrzędna Y</param>
        /// <returns></returns>
        public virtual bool Exist(int xCoordinate, int yCoordinate)
        {
            return xCoordinate >= 0 && yCoordinate >= 0 && xCoordinate < XSize && yCoordinate < YSize;
        }
        /// <summary>
        /// Konwertuje siatkę na string
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < YSize; j++)
            {
                for (int i = 0; i < XSize; i++)
                {
                    if (this[i, j])
                    {
                        sb.Append('█');
                    }
                    else
                    {
                        sb.Append(' ');
                    }
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }
    }
}
